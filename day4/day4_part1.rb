# require 'DateTime'
require 'date'

date_arrays = []

input_data = File.readlines('input').map(&:chomp)

input_data.each do |x|
  res = /^\[(?<date>(\d|\-)*) (?<time>.*)\] (?<message>.*)/.match(x)

  # p res['date']
  # p res['time']
  # p res['message']
  date_str = "#{res['date']} #{res['time']}"
  date = DateTime.strptime(date_str, '%Y-%m-%d %H:%M')
  # date_arrays.push({})
  date_arrays.push({"date" => date, "time" => res['time'], "message" => res['message']})

end

sorted = date_arrays.sort_by {|k| k['date']}

current_guard = false
current_sleeptime = false

guard_info = {}

sorted.each do |x|
  guard_message = /#(?<guard>\d*)/.match(x['message'])
  if guard_message && guard_message['guard']
    current_guard = guard_message['guard']
    current_sleeptime = false
    unless guard_info[current_guard]
      guard_info[current_guard] = {
          "total_sleep" => 0,
          "sleep_minutes" => {}
      }
    end
  end

  minutes = x['time'].split(':')[1].to_i
  if x['message'].include? "falls asleep"
    p "Guard #{current_guard} is gaan slapen on #{minutes}"
    current_sleeptime = minutes
  end
  if x['message'].include? "wakes up"
    sleep_duration = minutes - current_sleeptime
    p "Guard #{current_guard} heeft geslapen #{sleep_duration} van #{current_sleeptime} tot #{minutes}"
    guard_info[current_guard]['total_sleep'] += sleep_duration

    (current_sleeptime...minutes).each do |sleep_minute|
      if guard_info[current_guard]['sleep_minutes'][sleep_minute]
        guard_info[current_guard]['sleep_minutes'][sleep_minute] += 1
      else
        guard_info[current_guard]['sleep_minutes'][sleep_minute] = 1
      end
    end

  end
end

max_sleep = 0
max_guard = false

guard_info.each do |guard, array|
  if array['total_sleep'] > max_sleep
    max_guard = guard
    max_sleep = array['total_sleep']
  end
end

puts "Guard that slept the most: #{max_guard}"

sleepy_guard = guard_info[max_guard]

max_sleep_minutes = 0
max_sleep_minute = false
sleepy_guard['sleep_minutes'].each do |minute, count|
  if count > max_sleep_minutes
    max_sleep_minutes = count
    max_sleep_minute = minute
  end
end

puts "En hij heeft in minuut #{max_sleep_minute} Het meeste geslapen"
res = max_guard.to_i * max_sleep_minute
puts "Result: #{max_guard} * #{max_sleep_minute} = #{res}"