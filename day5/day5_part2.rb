# Run with ruby day5_part2.rb | grep Result | sort -n -k 3,3

input_data = File.readlines('input').map(&:chomp)[0]
orginele_data = input_data
# input_data = input_data.downcase

# puts "Input #{orginele_data}"

('a'..'z').each do |remove_letter|
  # puts "Removing letter #{remove_letter}"

  input_data = orginele_data

  input_data = input_data.gsub(remove_letter.upcase, "")
  input_data = input_data.gsub(remove_letter, "")

  len = input_data.length
  # puts "Input: #{input_data}"
  loop = true
  while loop
    ('a'..'z').each do |x|
      # Caveat, when Aa -> Nothing happens..
      # Boom on Aa and aA
      str = "#{x.upcase}#{x}"
      input_data = input_data.sub(str, "")

      str = "#{x}#{x.upcase}"
      input_data = input_data.sub(str, "")
    end
    if len != input_data.length
      #Go again
      len = input_data.length
      # puts "Looping again : #{input_data}"
    else
      loop = false
    end
  end
  puts "Result #{remove_letter} #{input_data.length}\n"
end


# p input_data
# dabCBAcaDA