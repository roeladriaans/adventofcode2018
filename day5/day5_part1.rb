input_data = File.readlines('input').map(&:chomp)[0]

# input_data = input_data.downcase

len = input_data.length

while true
  ('a'..'z').each do |x|
    # Caveat, when Aa -> Nothing happens..
    # Boom on Aa and aA
    str = "#{x.upcase}#{x}"
    input_data = input_data.sub(str,"")

    str = "#{x}#{x.upcase}"
    input_data = input_data.sub(str,"")

  end
  if len != input_data.length
    #Go again
    len = input_data.length
    p "Looping again"
  else
    p "Result #{input_data.length}"
    exit
  end

end

p input_data.length

# p input_data
# dabCBAcaDA