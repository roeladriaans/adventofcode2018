
def do_calc
  running_value = 0
  visited_numbers = {}
  while true
    File.open("input").each do |line|
      op = line[0]
      value = line[1..-1]

      if op == "+"
        running_value += value.to_i
      elsif op == "-"
        running_value -= value.to_i
      end

      # add the total to the list
      if visited_numbers.has_key?(running_value)
        puts "#{running_value} is already in the list"
        visited_numbers[running_value] = visited_numbers[running_value] + 1
        exit
      else
        visited_numbers[running_value] = 1
      end
    end
  end
end

do_calc