running_value = 0

File.open("input").each do |line|
  op = line[0]
  value = line[1..-1]
  puts "Op: #{op} - value #{value}"
  if op == "+"
    running_value += value.to_i
  elsif op == "-"
    running_value -= value.to_i
  end
end

puts "Total: #{running_value}"
