require 'set'

size = 1000
grid = Array.new(size) do
  Array.new(size, 0)
end

grid_names = Array.new(size) do
  Array.new(size, 0)
end

clean_ids = Set[]
dirty_ids = Set[]

ids = File.readlines('input').map(&:chomp)

ids.each do |x|
  res = /(?<id>#\d*) @ (?<x>\d{0,3}),(?<y>\d{0,3}): (?<l>\d{0,3})x(?<r>\d{0,3})/.match(x)
  p "Working on #{res['id']} -- #{x}"
  x = res['x'].to_i
  y = res['y'].to_i
  r = res['l'].to_i
  l = res['r'].to_i

  # Stap 1. :)
  (y...y + l).each do |yl|
    (x...x + r).each do |xr|
      grid[xr][yl] += 1
      if grid_names[xr][yl] == 0
        # Er zit nog niets op dit grid. We kunnen dit id dus aan clean_ids toevoegen,
        # mits deze nog niet in dirty_ids zit!
        grid_names[xr][yl] = [res['id']]
        clean_ids.add(res['id']) unless dirty_ids.include?(res['id'])
      else
        grid_names[xr][yl].push(res['id'])
        grid_names[xr][yl].each do |dirty_id|
          dirty_ids.add(dirty_id)
          clean_ids.delete(dirty_id)
        end
      end


    end
  end

end

p "Clean ids: #{clean_ids}"