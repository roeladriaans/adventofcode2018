grid = Array.new(999) {
  Array.new(999, 0)
}

ids = File.readlines('test').map(&:chomp)

ids.each do |x|
  res = /(?<id>#\d*) @ (?<x>\d{0,3}),(?<y>\d{0,3}): (?<l>\d{0,3})x(?<r>\d{0,3})/.match(x)
  p "Working on #{res['id']} -- #{x}"
  x = res['x'].to_i
  y = res['y'].to_i
  r = res['l'].to_i
  l = res['r'].to_i

  # Stap 1. :)
  # grid[x][y] += 1
  (y...y + l).each do |yl|
    (x...x + r).each do |xr|
      # p "#{xr}, #{yl}"
      grid[xr][yl] += 1
    end
  end
end

count = 0
grid.each do |x|
  x.each do |y|
    if y >= 2
      count += 1
    end
  end
end
p count
