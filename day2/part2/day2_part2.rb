
# Source: https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Ruby
def levenshtein(first:, second:)
  matrix = [(0..first.length).to_a]
  (1..second.length).each do |j|
    matrix << [j] + [0] * (first.length)
  end

  (1..second.length).each do |i|
    (1..first.length).each do |j|
      if first[j - 1] == second[i - 1]
        matrix[i][j] = matrix[i - 1][j - 1]
      else
        matrix[i][j] = [
            matrix[i - 1][j],
            matrix[i][j - 1],
            matrix[i - 1][j - 1],
        ].min + 1
      end
    end
  end
  return matrix.last.last
end

set_1 = []
set_2 = []

File.open('input').each do |line|
  set_1.push(line)
  set_2.push(line)
end

match = 1
while true
  set_1.each do |one|
    puts "New batch: #{one}"
    set_2.each do |two|
      res = levenshtein(first: one, second: two)
      if res == match
        puts "#{one} vs #{two} -> #{res}"
        exit
      else
        puts "no match #{one} vs #{two} -> #{res}"
      end
    end
  end
  match += 1
  puts "Updating match to #{match}"
end
