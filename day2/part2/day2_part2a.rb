ids = File.readlines('test').map(&:chomp)
# two = 0
# three = 0
# ids.each do |id|
#   counts = id.chars.group_by(&:itself).values.map(&:length).uniq
#   two += 1 if counts.include? 2
#   three += 1 if counts.include? 3
# end
# checksum = two * three
# puts "Part 1: #{checksum}"
#
ids.product(ids) do |x, y|
  # Wat doet hij hier:
  # zip x met y chars:
  # [[x,x], [y,y], [b,b], [a,x], [q, q]]
  # map:
  same = x.chars.zip(y.chars)

  # Select: Loop over met a en b, en
  # geef als ze hetzelfde zijn.
  # result is dus: # [[x,x], [y,y], [b,b], [q, q]]
  # Verschillende zijn er dus uit!
  same = same.select { |a, b| a == b }
  # Map &:first : loop over same, en geef 1e result uit array terug
  # result : [x, y, b, q]
  same = same.map(&:first)
  if same.length == x.length - 1
    puts "Part 2: #{same.join}"
    break
  end
end

