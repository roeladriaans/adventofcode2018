def build_array(c, visited_letters)
  visited_letters[c] =
    if visited_letters.key?(c)
      visited_letters[c] + 1
    else
      1
    end
end

count_two_letter = 0
count_three_letter = 0

File.open('input').each do |line|
  visited_letters = {}
  chars = line.split('')

  chars.each do |c|
    build_array(c, visited_letters)
  end

  has_two = false
  has_three = false

  visited_letters.each do |letterarray|
    if letterarray[1] == 2 && !has_two
      count_two_letter += 1
      has_two = true
    end
    if letterarray[1] == 3 && !has_three
      count_three_letter += 1
      has_three = true
    end
  end
end
puts "Total 2: #{count_two_letter}"
puts "Total 3: #{count_three_letter}"
res = count_three_letter * count_two_letter
puts "Result #{res}"
